<?php

namespace picof\utils;

class HttpRequest{
	
	protected $method, $script_name, $request_uri, $query, $get;

	public function __construct (){ 
		$this->method=$_SERVER['REQUEST_METHOD']; //$_REQUEST
		$this->script_name=$_SERVER['SCRIPT_NAME'];
		$this->request_uri=$_SERVER['REQUEST_URI'];
		$this->query=$_SERVER['QUERY_STRING'];
		$this->get=$_GET;
	}

	public function __set($a,$s){
		if(property_exists($this, $a)){
			$this->$a=$s;
		}
		else throw new \Exception ("invalid property") ;
	}

		public function __get($a){
		if(property_exists($this, $a)){
			return $this->$a;
		}
		else throw new \Exception ("invalid property") ;
	}

	public function getPathInfo(){
		/**$s=dirname($this->script_name);

		$c=str_replace($s, '', $this->request_uri);**/
		return $this->request_uri;
	}	
}