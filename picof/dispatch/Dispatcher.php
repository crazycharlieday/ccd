<?php

namespace picof\dispatch;

class Dispatcher{

	public $requete,$routes=array();

	public function __construct($r=null){
		$this->requete=$r;
	}

	public function addRoute($url,$controler,$action){
		$this->routes[$url]=array('c'=>$controler, 'a' => $action) ;
	}

	public function dispatch(){
		$path=$this->requete->getPathInfo();
		foreach ($this->routes as $key => $value) {
			//strstr($path,$key) 
			if(strstr($path,$key) ){
				$controler=$value['c'];
				$method=$value['a'];
				$c=new $controler($this->requete);
				echo $c->$method();
				break;
			}
		}
		if(!isset($c)){
			echo 'erreur 404';
		}
	}
}