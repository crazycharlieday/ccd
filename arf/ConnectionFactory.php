<?php
namespace arf;

use Illuminate\Database\Capsule\Manager as DB;

class ConnectionFactory{

    private static $config=null;
    private static $db=null;

    private static function setConfig( $file ){
        self::$config = parse_ini_file($file);
    }

    public static function makeConnection(){
        if (is_null(self::$db)){
            if (is_null(self::$config)){
                self::setConfig('./db.config.ini');
            }
            $db=new DB();

            $db->addConnection(array (
 			'driver' => self::$config['db_driver'],
 			'host' => self::$config['host'],
 			'database' => self::$config['dbname'],
 			'username' => self::$config['db_user'],
 			'password' => self::$config['db_password'],
 			'charset' => self::$config['charset'],
 			'collation' => self::$config['collation'],
 			'prefix' => ''
			));
			$db->setAsGlobal();
			$db->bootEloquent();
        }
        return self::$db;
	}

}