<?php
/**
 * Created by IntelliJ IDEA.
 * User: antoine
 * Date: 05/03/2015
 * Time: 10:48
 */

namespace homeapp\vue;


class vueHomeGreen {

    public $produits,$message;

    public function __construct ($tab,$m=NULL){
        $this->produits=$tab;
        $this->message=$m;
    }

    public function render($p){
        $s=$this->header();
        if($p==0){
                $s.=$this->accueil();
        }else {
            $s.='<section>
		<div class="container">
			<div class="row">';
            $s.=$this->nav();
            if ($p == 1) {
                $s.=$this->details();
            }else if($p==2){
                $s.=$this->liste();
            }
        }
        $s.=$this->footer();

        echo $s;
    }

    public function header(){

        $s='<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>HGreenH</title>
    <link href="ressources/css/bootstrap.min.css" rel="stylesheet">
    <link href="ressources/css/font-awesome.min.css" rel="stylesheet">
    <link href="ressources/css/prettyPhoto.css" rel="stylesheet">
    <link href="ressources/css/price-range.css" rel="stylesheet">
    <link href="ressources/css/animate.css" rel="stylesheet">
	<link href="ressources/css/main.css" rel="stylesheet">
	<link href="ressources/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="ressources/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ressources/images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ressources/images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ressources/images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ressources/images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header"><!--header-->
		

		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="/"><img src="ressources/images/home/logo.png" alt="" width="139" height="78"/></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li><a href="/compte/"><i class="fa fa-user"></i>Compte</a></li>
								<li><a href="/panier/"><i class="fa fa-shopping-cart"></i> Panier</a></li>
								<li><a href="/connection/"><i class="fa fa-lock"></i> Connection</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->

		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="index.html" class="active">Accueil</a></li>
								<li><a href="mailto:jeremy.petitcolin@gmail.com">Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->';
        return $s;
    }

    public function footer(){
        $s='			</div>
		</div>
	</section>
	<footer id="footer"><!--Footer-->
		<div class="footer_top"><!--footer_top-->
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								<li><a href="#"><i class="fa fa-phone"></i> 06 38 28 11 27</a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> info@ghg.com</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/footer_top-->

		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p class="pull-left">Copyright © 2013 E-SHOPPER Inc. All rights reserved.</p>
					<p class="pull-right">Designed by <span>Green Team</a></span></p>
				</div>
			</div>
		</div>

	</footer><!--/Footer-->



    <script src="ressources/js/jquery.js"></script>
	<script src="ressources/js/bootstrap.min.js"></script>
	<script src="ressources/js/jquery.scrollUp.min.js"></script>
	<script src="ressources/js/price-range.js"></script>
    <script src="ressources/js/jquery.prettyPhoto.js"></script>
    <script src="ressources/js/main.js"></script>
</body>
</html>';
        return $s;
    }

    public function nav(){
        $s='<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Categories</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->

						';
                        foreach(\homeapp\model\Pieces::all() as $piece){
                            $s.='<div class="panel panel-default">
								    <div class="panel-heading">
									    <h4 class="panel-title"><a href="/Categorie?piece='.$piece->id.'">'.$piece->nom.'</a></h4>
								    </div>
							    </div>';
                        }
						$s.='</div><!--/category-products-->
                            <div class="brands_products"><!--brands_products-->
							<h2>Matériaux</h2>
							<div class="brands-name">
								<ul class="nav nav-pills nav-stacked">';
        foreach(\homeapp\model\Types::all() as $type){
                $s.='<li><a href=/Categorie?type='.$type->id.'> '.$type->type.'</a></li>';
            }
								$s.='</ul>
							</div>
						</div><!--/brands_products-->
					</div>
				</div>';
        return $s;
    }
    public function accueil(){
        $s='<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>

						<div class="carousel-inner">
							<div class="item active">
								<div class="col-sm-6">
									<h1><span> </span> </h1>
									<h2>Recyclez votre intérieur!</h2>
									<p>Avec Home Green Home redonnez une seconde vie à vos objets oubliés.
									Ecologiques et esthétiques, nos meubles redonneront du cachet à votre intérieur grace à des objets design tout en gardant leur propre identité</p>
									
								</div>
								<div class="col-sm-6">
									<img src="ressources/images/home/logo2.png" class="girl img-responsive" alt="" />
									
								</div>
							</div>
							<div class="item">
								<div class="col-sm-6">
									<h1><span> </span> </h1>
									<h2>Nouvel Arrivage!</h2>
									<p>Aujourd\'hui, Home Green Home est heureux de vous présenter son nouvel objet: Une magnifique chaise en bois. Cette chaise convient parfaitement aux interieurs ainsi qu\'aux exterieurs grâce à son design épuré.</p>
									
								</div>
								<div class="col-sm-6">
									<img src="ressources/images/home/fauteuil.png" class="girl img-responsive" alt="" />
									
								</div>
							</div>

							<div class="item">
								<div class="col-sm-6">
									<h1><span> </span> </h1>
									<h2>HGH participe à vos projets!</h2>
									<p>Home Green Home participe aussi à vos projets. En effet, les plus beaux objets créés à partir de matériaux recyclés seront choisis par toute l\'équipe de HGH et fabriqués dans nos ateliers.
 </p>
								
								</div>
								<div class="col-sm-6">
									<img src="ressources/images/home/armure.png" class="girl img-responsive" alt="" />
						
								</div>
							</div>

						</div>

						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>

				</div>
			</div>
		</div>
	</section><!--/slider-->

	<section>
		<div class="container">
			<div class="row">';

                $s.=$this->nav();
				$s.='<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Meilleures ventes</h2>';

        foreach($this->produits[0] as $p){

            $s.='<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="ressources/images/'.$p->photo.'" alt="" />
										<h2>'.$p->prix.'€</h2>
										<p>'.$p->nom.'</p>
										<p>nombre de commande : '.$p->nb_commande.'<p>
										<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Ajouter au panier</a>
									</div>
									<div class="product-overlay">
										<div class="overlay-content">
										    <h3>'.$p->description.'</h3>
											<h2>'.$p->prix.'€</h2>
											<p>'.$p->nom.'</p>
											<a href="/details?id='.$p->id.'" class="btn btn-default add-to-cart"><i class="fa fa-plus"></i>Details</a>
											<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Ajouter au panier</a>
										</div>
									</div>
								</div>
							</div>
						</div>';
        }
					$s.='</div><!--features_items-->


					<div class="recommended_items"><!--recommended_items-->
						<h2 class="title text-center">Produits les plus appréciés</h2>

						<div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner">
								<div class="item active">

									';
        $k=0;
        foreach($this->produits[1] as $p) {
            $k++;
            if($k==4){
                $s.='<div class="item">';
            }
            $s .= '<div class="col-sm-4">
										<div class="product-image-wrapper">
											<div class="single-products">
												<div class="productinfo text-center">
													<img src="ressources/images/' . $p->photo . '" alt="" />
													<h2>' . $p->prix . '€</h2>
													<p>' . $p->nom . '</p>
													<p> nombre de vote : '.$p->nb_jaime.'<p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Ajouter au panier</a>
												</div>
                                                <div class="product-overlay">
										<div class="overlay-content">
										    <h3>'.$p->description.'</h3>
											<h2>'.$p->prix.'€</h2>
											<p>'.$p->nom.'</p>
											<a href="/details?id='.$p->id.'" class="btn btn-default add-to-cart"><i class="fa fa-plus"></i>Details</a>
											<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Ajouter au panier</a>
										</div>
									</div>
											</div>
										</div>
									</div>';



            if($k==3){
                $s.='</div>';
            }
        }
        $s.='</div>
							</div>
							 <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
								<i class="fa fa-angle-left"></i>
							  </a>
							  <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
								<i class="fa fa-angle-right"></i>
							  </a>
						</div>
					</div><!--/recommended_items-->

				</div>
';
        return $s;
    }

    public function details(){

        $s='
                <div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<img src="ressources/images/'.$this->produits->photo.'" alt="">
								<h3>ZOOM</h3>
							</div>

						</div>
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								<h2>'.$this->produits->nom.'</h2>
								<img src="ressources/images/product-details/rating.png" alt="">
								<span>
									<span>'.$this->produits->prix.'€</span>
									<label>Quantité:</label>
									<input type="text" value="3">
									<button type="button" class="btn btn-fefault cart">
										<i class="fa fa-shopping-cart"></i>
										Ajouter au panier
									</button>
									<p>nombre de j\'aime: '.$this->produits->nb_jaime.'</p>
									<a href="/jaime?id='.$this->produits->id.'"><button type="button" class="btn_jaime">J\'aime !</button></a>
								</span>
								<p><b>Disponibilité:</b> En Stock</p>
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->

					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li><a href="#details" data-toggle="tab">Details</a></li>
								<li class="active"><a href="#reviews" data-toggle="tab">Reviews (5)</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade" id="details">
								<div class="col-sm-12">
									<p>'.$this->produits->description.'</p>
								</div>
							</div>


							<div class="tab-pane fade active in" id="reviews">
								<div class="col-sm-12">
									<ul>
										<li><a href=""><i class="fa fa-user"></i>EUGEN</a></li>
										<li><a href=""><i class="fa fa-clock-o"></i>12:41 PM</a></li>
										<li><a href=""><i class="fa fa-calendar-o"></i>31 DEC 2014</a></li>
									</ul>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
									<p><b>Write Your Review</b></p>

									<form action="#">
										<span>
											<input type="text" placeholder="Your Name">
											<input type="email" placeholder="Email Address">
										</span>
										<textarea name=""></textarea>
										<b>Rating: </b> <div id="rate"></div>
										<button type="button" class="btn btn-default pull-right">
											Submit
										</button>
									</form>
								</div>
							</div>

						</div>
					</div><!--/category-tab-->

				</div>
				';
        return $s;
    }

    public function liste(){
        $s='				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>

						';
        foreach($this->produits as $p){
            $s.='<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
									<div class="productinfo text-center">
										<img src="ressources/images/'.$p->photo.'" alt="" />
										<h2>'.$p->prix.'€</h2>
										<p>'.$p->nom.'</p>
										<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Ajouter au panier</a>
									</div>
									<div class="product-overlay">
										<div class="overlay-content">
										    <h3>'.$p->description.'</h3>
											<h2>'.$p->prix.'€</h2>
											<p>'.$p->nom.'</p>
											<a href="/details?id='.$p->id.'" class="btn btn-default add-to-cart"><i class="fa fa-plus"></i>Details</a>
											<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Ajouter au panier</a>
										</div>
									</div>
								</div>
							</div>
						</div>';
        }

        $s.='</div><!--features_items-->
				</div>
			</div>
		</div>
	</section>';
        return $s;
    }
}