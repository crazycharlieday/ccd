<?php
/**
 * Created by IntelliJ IDEA.
 * User: antoine
 * Date: 05/03/2015
 * Time: 10:48
 */

namespace homeapp\vue;


class vueClient {

    public $message;

    public function __construct ($m=NULL){
        $this->message=$m;
    }

    public function render($p){
    	$vue= new \homeapp\vue\vueHomeGreen(array());
    	$s=$vue->header();
    	if($p==0){
    		$s.=$this->identification();
    	}
        $s.=$vue->footer();

        echo $s;
    }

    public function identification(){
    	$s='
    	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Login to your account</h2>
						<form action="#">
							<input type="text" placeholder="Name" />
							<input type="email" placeholder="Email Address" />
							<span>
								<input type="checkbox" class="checkbox"> 
								Keep me signed in
							</span>
							<button type="submit" class="btn btn-default">Login</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">OR</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>New User Signup!</h2>
						<form action = "/inscription" method = "post">
						    <input type="text" name="login" placeholder="login" required/>
							<input type="text" name="nom" placeholder="Nom" required/>
							<input type="text" name="prenom" placeholder="Prenom" required/>
							<input type="email" name="email" placeholder="Email " required/>
							<input type="password" name="password" placeholder="Mot de passe" required/>
							<button type="submit" class="btn btn-default">Inscription</button>
						</form>
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->';
	return $s;
    }
}