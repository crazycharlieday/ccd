<?php

namespace homeapp\model;

class Types extends \Illuminate\Database\Eloquent\Model {
	protected $table ='ccd_types' ;
	protected $primaryKey ='id' ;
	public $timestamps= false;

	public function item(){
		return $this->hasMany('homeapp\model\Items','type_id');
	}
}