<?php

namespace homeapp\model;

class Pieces extends \Illuminate\Database\Eloquent\Model {
	protected $table ='ccd_pieces' ;
	protected $primaryKey ='id' ;
	public $timestamps= false;

	public function item() {
		return $this->hasMany('homeapp\model\Items','piece_id');
	}

	
}