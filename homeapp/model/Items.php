<?php

namespace homeapp\model;

class Items extends \Illuminate\Database\Eloquent\Model {
	protected $table ='ccd_items' ;
	protected $primaryKey ='id' ;
	public $timestamps= false;

    public function piece() {
        return $this->belongsTo('homeapp\model\Pieces','piece_id');
    }

    public function type(){
        return $this->belongsTo('homeapp\model\Types','type_id');
    }
}