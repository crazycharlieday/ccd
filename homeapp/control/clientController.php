<?php
/**
 * Created by IntelliJ IDEA.
 * User: antoine
 * Date: 05/03/2015
 * Time: 15:16
 */

namespace homeapp\control;


class clientController {
    public $requete;

    public function __construct($r){
        $this->requete=$r;
    }


    public function accueil(){
        $vue = new \homeapp\vue\vueClient() ;
        $vue->render(0);
    }
    public function login(){

        if(!isset($_SESSION['co'])) {
            if (isset($_POST['connect'])){
                #htmlentities est la pour une securite et trim est la pour eviter les espaces dans le usrename
                $user =htmlentities(trim($_POST['user']));
                $password =htmlentities(trim($_POST['password']));


                $dbb = \arf\ConnectionFactory::makeConnection();

                $employe= \homeapp\model\Client::where('login', '=', $user)->first() ;


                if(isset($employe) && password_verify($password,$employe->password)){


                    //creation de session
                    $_SESSION['co']=1;
                    unset($employe->password);
                    $_SESSION['ep']=serialize($employe);
                    header('Location:/compte');
                }else {
                    echo '<div class="alert alert-warning" role="alert">
                            <strong>Attention!</strong> identifiant ou mot de passe incorrecte </div>';
                    $this->accueil();
                }
            }else{
                $this->accueil();
            }

        }else {
            $vue = new \homeapp\vue\vueHomeGreen(array()) ;
            $vue->render(0);
        }
    }

    public function logout(){
        if(session_destroy()){
            header('Location:/');
        }
    }

    public function inscription(){
        if(!isset($_SESSION['co']) && isset($_POST['inscrit'])) {
            $m='';
            $dbb = \arf\ConnectionFactory::makeConnection();
            $client= new \homeapp\model\Client();
            $client->username=filter_var($_POST['login'],FILTER_SANITIZE_STRING);
            $client->password=password_hash($_POST['password'],PASSWORD_DEFAULT,array('cost'=>12));
            $client->nom=filter_var($_POST['nom'],FILTER_SANITIZE_STRING);
            $client->prenom=filter_var($_POST['prenom'],FILTER_SANITIZE_STRING);
            $client->email=filter_var($_POST['email'],FILTER_SANITIZE_EMAIL);
            if ($client->save()) {
                $m= '<p><div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <b>Merci de vous être inscrit!</b>
                                    </div></p>';

            }
            unset($_POST);
            $this->accueil();
        } else {
            $vue = new \homeapp\vue\vueHomeGreen(array()) ;
            $vue->render(0);
        }
    }
}