<?php
/**
 * Created by IntelliJ IDEA.
 * User: antoine
 * Date: 05/03/2015
 * Time: 10:45
 */

namespace homeapp\control;


class homeController {
    public $requete;

    public function __construct($r){
        $this->requete=$r;
    }

    public function accueil(){
        $dbb= \arf\ConnectionFactory::makeConnection();
        $itemsr = \homeapp\model\Items::orderBy('nb_commande','DESC')->take(3)->get();
        $itemsj = \homeapp\model\Items::orderBy('nb_jaime','DESC')->take(6)->get();
        $tab=array($itemsr,$itemsj);
        $vue = new \homeapp\vue\vueHomeGreen($tab) ;
        $vue->render(0);
    }

    public function vueDetaille(){
        if(isset($_GET['id'])){
            $dbb= \arf\ConnectionFactory::makeConnection();
            $item=\homeapp\model\Items::find($_GET['id']);
            $vue = new \homeapp\vue\vueHomeGreen($item);
            $vue->render(1);
        }
    }

    public function vueCategorie(){
        $dbb= \arf\ConnectionFactory::makeConnection();
        if(isset($_GET['piece'] ) && !isset($_GET['type'] )){
            $piece=\homeapp\model\Pieces::find($_GET['piece']);
            $items=$piece->item;
        }
        if(isset($_GET['type']) && !isset($_GET['piece'] )){
            $type=\homeapp\model\Types::find($_GET['type']);
            $items=$type->item;
        }
        if(isset($_GET['type']) && isset($_GET['piece'] )){
            $piece=\homeapp\model\Pieces::find($_GET['piece']);
            $items=$piece->item;
            $items=\homeapp\model\Items::where("type_id","=",$_GET['type'])->where("piece_id","=",$_GET['piece'])->get();
        }
        $vue=new \homeapp\vue\vueHomeGreen($items);
        $vue->render(2);

        //
    }
    public function jaime(){
        if(isset($_GET['id'])){
            $dbb= \arf\ConnectionFactory::makeConnection();
            $item= \homeapp\model\Items::find($_GET['id']);
            $item->nb_jaime+=1;
            $item->save();
            header("Location:/details?id=".$_GET['id']);

        }
    }
}